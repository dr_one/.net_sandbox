﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Tools.Performance {

	public static class Benchmark {

		[DebuggerStepThrough]
		public static double Run( Action func, int iterations, string description = "" ) {

			Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;
			Thread.CurrentThread.Priority = ThreadPriority.Highest;

			func();

			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();

			var watch = Stopwatch.StartNew();
			watch.Start();
			for( int i = 0; i < iterations; i++ ) {
				func();
			}
			watch.Stop();

			Console.Write( description );
			Console.WriteLine( " Time Elapsed {0} ms", watch.Elapsed.TotalMilliseconds );

			return watch.Elapsed.TotalMilliseconds;
		}

	}
}
