﻿Problem:
.NET uses Reflection when performs ToString for Enum class. This might be bottleneck (e.g. for heavy serialization).

Idea:
Create generic solution: for each enum type create helper type for caching string representations of enum's values.

Results:
Optimized version is working ~20x times faster and ~4x times slower then best possible solution

TODO:
1) Add [Flags] support
2) Add (Try)Parse methods 
3) Test on different types of enum (long, flags, sparse etc)

 