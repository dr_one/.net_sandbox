﻿using System;
using System.Collections.Generic;

namespace Sandbox.EnumToString {

	public static class EnumExtensions {

		public static string ToStringOptimized<TEnum>( this TEnum e ) where TEnum : struct, IConvertible {
			return EnumHelper<TEnum>.ToString( e );
		}

	}

	public static class EnumHelper<T> where T : struct {

		private static readonly Dictionary<T, string> Value2Name = new Dictionary<T, string>();

		static EnumHelper() {

			var type = typeof( T );

			if( !type.IsEnum )
				throw new ArgumentException( "T must be an enumerated type" );

			var names = Enum.GetNames( type );
			var values = (T[])Enum.GetValues( type );

			for( int i = 0; i < names.Length; i++ ) {
				Value2Name.Add( values[i], names[i] );
			}

		}

		public static string ToString( T item ) {
			return Value2Name[item];
		}

	}

}
