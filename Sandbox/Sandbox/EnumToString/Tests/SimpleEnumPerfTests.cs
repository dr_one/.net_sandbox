﻿using NUnit.Framework;
using Tools.Performance;

namespace Sandbox.EnumToString.Tests {

	public enum SimpleEnum {

		None = 0,
		One,
		Two,
		Three,
		Four

	}

	[TestFixture]
	public sealed class SimpleEnumPerfTests {

		[Test]
		public void Default_SimpleEnum_Test() {
			Benchmark.Run( () => { SimpleEnum.Three.ToString(); }, 1000000 );
		}

		[Test]
		public void Optimized_SimpleEnum_Test() {
			Benchmark.Run( () => { SimpleEnum.Three.ToStringOptimized(); }, 1000000 );
		}

		[Test]
		public void Best_SimpleEnum_Test() {
			Benchmark.Run( () => { "Three".ToString(); }, 1000000 );
		}

	}

}
